<?php

/* default/template/extension/payment/pagseguro_boleto.twig */
class __TwigTemplate_e58a0e1cbdd37b05d4953e1fa24019c8a2920bbde3a195b3770d2bf12f7a40a7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["warning"]) ? $context["warning"] : null)) {
            // line 2
            echo "<div class=\"alert alert-danger\" id=\"warning\" role=\"alert\">";
            echo (isset($context["warning"]) ? $context["warning"] : null);
            echo "</div>
";
        } else {
            // line 4
            echo "<div id=\"warning\" class=\"alert alert-danger\" role=\"alert\" style=\"display:none\"></div>


<div id=\"info\" class=\"alert alert-info\" role=\"alert\" style=\"display:none\">Aguarde....</div>

<div class=\"form-horizontal col-sm-offset-3\">
  <div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">CPF</label>
    <div class=\"col-sm-3\">
      <input type=\"text\" name=\"cpf\" id=\"cpf\" value=\"";
            // line 13
            echo (isset($context["cpf"]) ? $context["cpf"] : null);
            echo "\" class=\"form-control\" />
    </div>
  </div>
  
  <div class=\"form-group\">
    <div class=\"col-sm-10 col-sm-offset-2\">
      <button type=\"button\" id=\"button-confirm\" class=\"btn btn-primary\" data-loading-text=\"Aguarde...\">
        <i class=\"fa fa-barcode\"></i> 
        Imprimir Boleto
      </button>
    </div>
  </div>
</div>

<script type=\"text/javascript\" src=\"catalog/view/javascript/pagseguro/colorbox/jquery.colorbox-min.js\"></script>
<link href=\"catalog/view/javascript/pagseguro/colorbox/colorbox.css\" rel=\"stylesheet\" media=\"all\" />

<script type=\"text/javascript\">
if (typeof(PagSeguroDirectPayment) == 'undefined') {
    alert('Erro ao carregar javascript.\\nAcesse http://www.valdeirsantana.com.br / Procure pelo módulo / Clique na aba FAQ para obter mais informações.');
}

PagSeguroDirectPayment.setSessionId('";
            // line 35
            echo (isset($context["session_id"]) ? $context["session_id"] : null);
            echo "');

\$('#button-confirm').click(function(){
    
    \$('#warning').empty().hide();
    
    \$.ajax({
        url: 'index.php?route=extension/payment/pagseguro_boleto/transition',
        type: 'POST',
        data: 'senderHash=' + PagSeguroDirectPayment.getSenderHash() + '&cpf=' + \$('input#cpf').val(),
        dataType: 'JSON',
            beforeSend: function() {
                \$('#button-confirm').button('loading');
            },
            success: function(json){
            if (json.error){
                \$('#warning').html(json.error.message).show();
            } else {
                \$.colorbox({
                iframe:true,
                open:true,
                href:json.paymentLink,
                innerWidth:'90%',
                innerHeight:'90%',
                onClosed: function () {
                    
                    \$('.form-horizontal').remove();
                    \$('#info').show();
                    
                    \$.ajax({
                    url: 'index.php?route=extension/payment/pagseguro_boleto/confirm',
                    data: 'status=' + json.status + \"&order_id=\" + json.order_id,
                    type: 'POST',
                    success: function (){
                        setTimeout(function(){
                            location.href = '";
            // line 70
            echo (isset($context["continue"]) ? $context["continue"] : null);
            echo "';
                        }, 5000);
                    }
                    })
                }
                });
            }
        },
        complete: function(){
            \$('#button-confirm').button('reset');
        }
    });
});
</script>
";
        }
    }

    public function getTemplateName()
    {
        return "default/template/extension/payment/pagseguro_boleto.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 70,  63 => 35,  38 => 13,  27 => 4,  21 => 2,  19 => 1,);
    }
}
/* {% if warning %}*/
/* <div class="alert alert-danger" id="warning" role="alert">{{ warning }}</div>*/
/* {% else %}*/
/* <div id="warning" class="alert alert-danger" role="alert" style="display:none"></div>*/
/* */
/* */
/* <div id="info" class="alert alert-info" role="alert" style="display:none">Aguarde....</div>*/
/* */
/* <div class="form-horizontal col-sm-offset-3">*/
/*   <div class="form-group">*/
/*     <label class="col-sm-2 control-label">CPF</label>*/
/*     <div class="col-sm-3">*/
/*       <input type="text" name="cpf" id="cpf" value="{{ cpf }}" class="form-control" />*/
/*     </div>*/
/*   </div>*/
/*   */
/*   <div class="form-group">*/
/*     <div class="col-sm-10 col-sm-offset-2">*/
/*       <button type="button" id="button-confirm" class="btn btn-primary" data-loading-text="Aguarde...">*/
/*         <i class="fa fa-barcode"></i> */
/*         Imprimir Boleto*/
/*       </button>*/
/*     </div>*/
/*   </div>*/
/* </div>*/
/* */
/* <script type="text/javascript" src="catalog/view/javascript/pagseguro/colorbox/jquery.colorbox-min.js"></script>*/
/* <link href="catalog/view/javascript/pagseguro/colorbox/colorbox.css" rel="stylesheet" media="all" />*/
/* */
/* <script type="text/javascript">*/
/* if (typeof(PagSeguroDirectPayment) == 'undefined') {*/
/*     alert('Erro ao carregar javascript.\nAcesse http://www.valdeirsantana.com.br / Procure pelo módulo / Clique na aba FAQ para obter mais informações.');*/
/* }*/
/* */
/* PagSeguroDirectPayment.setSessionId('{{ session_id }}');*/
/* */
/* $('#button-confirm').click(function(){*/
/*     */
/*     $('#warning').empty().hide();*/
/*     */
/*     $.ajax({*/
/*         url: 'index.php?route=extension/payment/pagseguro_boleto/transition',*/
/*         type: 'POST',*/
/*         data: 'senderHash=' + PagSeguroDirectPayment.getSenderHash() + '&cpf=' + $('input#cpf').val(),*/
/*         dataType: 'JSON',*/
/*             beforeSend: function() {*/
/*                 $('#button-confirm').button('loading');*/
/*             },*/
/*             success: function(json){*/
/*             if (json.error){*/
/*                 $('#warning').html(json.error.message).show();*/
/*             } else {*/
/*                 $.colorbox({*/
/*                 iframe:true,*/
/*                 open:true,*/
/*                 href:json.paymentLink,*/
/*                 innerWidth:'90%',*/
/*                 innerHeight:'90%',*/
/*                 onClosed: function () {*/
/*                     */
/*                     $('.form-horizontal').remove();*/
/*                     $('#info').show();*/
/*                     */
/*                     $.ajax({*/
/*                     url: 'index.php?route=extension/payment/pagseguro_boleto/confirm',*/
/*                     data: 'status=' + json.status + "&order_id=" + json.order_id,*/
/*                     type: 'POST',*/
/*                     success: function (){*/
/*                         setTimeout(function(){*/
/*                             location.href = '{{ continue }}';*/
/*                         }, 5000);*/
/*                     }*/
/*                     })*/
/*                 }*/
/*                 });*/
/*             }*/
/*         },*/
/*         complete: function(){*/
/*             $('#button-confirm').button('reset');*/
/*         }*/
/*     });*/
/* });*/
/* </script>*/
/* {% endif %}*/
